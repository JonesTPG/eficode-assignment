FROM node:10

WORKDIR /usr/src/app

COPY front-end/package*.json ./front-end

RUN npm install

COPY front-end ./front-end

RUN npm run build:ui

COPY back-end/package*.json ./back-end

RUN npm install

COPY back-end ./back-end

EXPOSE 4000

CMD [ "npm", "run", "start" ]