# Eficode Timetables Assignment

This assignment is done with

## Live Demo

The live demo of the application can be found at

[https://eficode.kooditaiturit.fi](https://eficode.kooditaiturit.fi)

The application is running on AWS EC2 instance, where some of our other applications such as the Full Stack Open 2019 application.
(More information about it can be found [here](https://demo.kooditaiturit.fi))

The instance is setup with nginx reverse proxy, which allows us to host multiple Node.js applications on a single server (and deal with SSL more easily).

## The planning process

We decided to do this application, as all of us are looking for a job in the software industry. Because we have school and other projects, we decided to team up for this assignment and be as efficient as we can.

The planning started with taking a look at the necessary features and the 3rd party API we would need to use in our application. We made a document declaring the most important details and divided the work between us.

Since we already had made a working application with React, Apollo, Cypress and Material UI we had a good starting point to this assignment. We also know the basic principles of all of said technologies, though learning some of the most advanced patterns and features (such as Apollo Local State Management and advanced Cypress testing) are still a work in progress.

## Running the application

The application can be run locally be following these steps:

1. Clone the repository to a fresh folder.
2. Build the image with docker: **docker build -t eficode .**
3. Run the image with docker: **docker run -p 4000:4000 eficode**
4. Go to localhost:4000 with a browser.
