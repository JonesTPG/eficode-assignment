describe('Example Application Test ', function() {
  describe('Example app login', function() {
    beforeEach(function() {
      console.log('before each test run in this describe block');
    });

    it('Front page can be opened', function() {
      cy.visit(Cypress.env('BASE_REACT_URL'));
      cy.contains('Eficode');
      cy.url().should('eq', Cypress.env('BASE_REACT_URL') + '/');
    });
  });
});
