import React from 'react';
import { useTimetables } from '../hooks/timetables';
import { useTestStop } from '../hooks/testStop';
import CircularProgress from '@material-ui/core/CircularProgress';

const eficode = {
  lat: 60.198626,
  lon: 24.937843
};
const juustenintie = {
  lat: 60.24499,
  lon: 24.86876
};

const secondsToDisplay = unixTimestamp => {
  const dateObj = new Date(unixTimestamp * 1000);
  const utcString = dateObj.toUTCString();
  const time = utcString.slice(-11, -4);
  return `${time}`;
};
const totalDistance = legs => {
  let total = 0;
  legs.forEach(item => {
    total += item.distance;
  });
  return Math.round(total);
};

const Test2000 = () => {
  const data = useTimetables(eficode, juustenintie);
  const test = useTestStop();
  if (data.loading || test.loading) {
    return <CircularProgress />;
  }
  let itineraries = data.data.plan.itineraries;
  console.log(itineraries);
  return (
    <div>
      <p>
        5 next itineraries from {data.placeNames[0]} to {data.placeNames[1]}
      </p>
      <ul>
        {itineraries.map((item, index) => {
          return (
            <li key={index}>
              {`Walking distance ${Math.round(
                item.walkDistance
              )} meters   ||   ${secondsToDisplay(
                item.startTime / 1000
              )} ----> ${secondsToDisplay(
                item.endTime / 1000
              )}   ||   total distance ${totalDistance(item.legs)} meters`}
            </li>
          );
        })}
      </ul>
      <button onClick={() => data.refresh()}>Refresh</button>
      <button onClick={() => data.switchDirection()}>{'<-->'}</button>
    </div>
  );
};

export default Test2000;
