import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TechCard from './TechCard';
import FeatureCard from './FeatureCard';
import Button from '@material-ui/core/Button';

import { withRouter } from 'react-router-dom';

import { frontPage } from './pagedata';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.primary
  }
}));

const PageInfo = props => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <h1> Eficode Timetables Assignment</h1>
              <p>
                Application building started 19.02. with the Full Stack Theme
                Base, a demo application template we have built as a part of
                Full Stack Open 2019.
              </p>
              <h3>This Assignment is Done By</h3>
              <p>Janetta Huoponen (Front-End and E2E Tests)</p>
              <p>Markus Röman (Back-End logic and GraphQL queries)</p>
              <p>
                Joonas Ryynänen (DevOps features and production environment)
              </p>
              <Button
                variant="contained"
                onClick={() => props.history.push('/timetables')}
                color="secondary"
              >
                Check out the application here!
              </Button>
              <h2>Used technologies:</h2>
            </Paper>
          </Grid>

          {frontPage.techcards.map(data => (
            <Grid key={data.title} item xs={12} sm={6} md={3}>
              <TechCard data={data}></TechCard>
            </Grid>
          ))}
        </Grid>
      </div>
    </>
  );
};

export default withRouter(PageInfo);
