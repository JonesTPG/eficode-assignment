/* eslint-disable indent */
import React from 'react';
import clsx from 'clsx';
import { useTheme, withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';

import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';

import Brightness from '@material-ui/icons/Brightness4';

import { Switch, Route, withRouter } from 'react-router-dom';

import PageInfo from './page-info/PageInfo';
import MainStyles from './MainStyles';

const Main = ({ classes, ...props }) => {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleThemeChange = async () => {
    return;
  };

  const handleSideNavClick = route => {
    props.history.push(route);
    setOpen(false);
  };

  const Content = () => {
    return (
      <Switch>
        {/* <Route path="/timetables" render={() => <Timetables />} /> */}
        <Route path="/" render={() => <PageInfo />} />
      </Switch>
    );
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="static"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open
        })}
      >
        <Toolbar>
          <IconButton
            data-cy="menu"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>

          <IconButton onClick={handleThemeChange} color="inherit">
            <Brightness />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton
            onClick={handleDrawerClose}
            className={classes.chevronLeftButton}
          >
            {theme.direction === 'ltr' ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List disablePadding className={classes.list}>
          <ListItem button onClick={() => handleSideNavClick('/')}>
            <ListItemIcon>
              <HomeIcon className={classes.sidenavIcon} />
            </ListItemIcon>
            <ListItemText primary="Home" />
          </ListItem>

          <ListItem
            data-cy="timetables"
            button
            onClick={() => handleSideNavClick('/timetables')}
          >
            <ListItemIcon>
              <InboxIcon className={classes.sidenavIcon} />
            </ListItemIcon>
            <ListItemText primary="Timetables" />
          </ListItem>
          <Divider />
        </List>
      </Drawer>

      <main
        className={clsx(classes.content, {
          [classes.contentShift]: open
        })}
      >
        {Content()}
      </main>
    </div>
  );
};

const styledMain = withStyles(MainStyles)(Main);
export default withRouter(styledMain);
