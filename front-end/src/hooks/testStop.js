import { TEST_STOP } from '../queries/route';
import { useQuery } from '@apollo/react-hooks';

export const useTestStop = () => {
  const { loading, error, data, refetch } = useQuery(TEST_STOP, {
    variables: { id: 'HSL:1173434' }
  });

  return {
    loading,
    error,
    data,
    refetch
  };
};
