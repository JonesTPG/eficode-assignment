import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import Main from './components/main/Main';
import { useQuery } from '@apollo/react-hooks';

import { mainTheme, darkTheme } from './AppStyles';
import Test2000 from './components/Test2000';

const App = () => {
  return (
    <>
      <Router>
        <ThemeProvider theme={mainTheme}>
          <Switch>
            <Route path="/test" render={() => <Test2000 />} />
            <Route path="/" render={() => <Main />} />
          </Switch>
        </ThemeProvider>
      </Router>
    </>
  );
};

export default App;
